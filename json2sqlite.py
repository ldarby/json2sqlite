#!/usr/bin/env python

# Copyright 2015 Laurence Darby

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import re
import sqlite3
import json

def detect_type (name, value):
    #json.load does some type detection already, this prepares the types for sqlite
    #print "name = {}, value = {}, type v = {}\n".format (name,value, str(type(value)))
    if str(type (value)) == "<type 'bool'>":
        ret = "BOOLEAN"
    elif str(type (value)) == "<type 'float'>":
        ret = "FLOAT"
    elif re.match('.*date.*', name, re.IGNORECASE):
        if re.match ('^\d\d\d\d-\d\d-\d\d \d\d.*$', value):
            ret = "DATETIME"
        elif re.match ('^\d\d\d\d-\d\d-\d\d*$', value):
            ret = "DATE"
        elif value == "None":
            ret = "DATETIME" #just assuming it...
        else:
            ret = "TEXT" #some fields labled with "date" might not be dates at all
    elif re.match('^\d+$', value):
        ret =  "INTEGER"
    else:
        ret = "TEXT"
    #print "detect_type: {} =>{}<= = {}\n".format (name, value, ret)
    return ret

if len (sys.argv) > 4 or len (sys.argv) < 4 or sys.argv[1] == "-h" or sys.argv[1] == "--help":
    print "Usage: {} <Table name> <input json file> <output sqlite file>".format (sys.argv[0])
    exit(0)

table_name = sys.argv[1]
infile = sys.argv[2]
outfile = sys.argv[3]

conn = sqlite3.connect(outfile)
conn.text_factory = str

c = conn.cursor()
c.execute("SELECT name FROM sqlite_master WHERE type='table';")
tables = c.fetchall()

try:
    fd = open (infile, "r")
    objects = json.load (fd)
except Exception as e:
    print e
    exit(0)

have_db = False
for i in tables:
    if table_name == i[0]:
        have_db = True
if not have_db:
    create = "CREATE TABLE {} ( ".format (table_name)
    for field in objects[0]:
        if field == "type":
            #My data source had both lower and upper case Type fields.  The
            # lower case one has the same value as table_name, so ignore it.
            continue
        create += "{} {}, ".format(field, detect_type(field, objects[0][field]))
    create = create [:-2]
    create += " )"
    conn.execute (create);
    conn.commit()

values = ()
for obj in objects:
    insert_str = "INSERT into {} VALUES (".format (table_name)
    for field in obj:
        if field == "type":
            continue
        try:
            if str(type (obj[field])) == "<type 'unicode'>":
                t = obj[field].encode('utf-8',errors='replace')
            else:
                t = str(obj[field])
            t = (t, )
            
        except Exception as e:
            t = (str("Fixme: script that generated this text for field {} failed due to exception: {}".format(field,str(e))), )
            print str(e)
            
        values += t
        insert_str += "?, "

    insert_str = insert_str [:-2]
    insert_str += ")"
    try:
        conn.execute(insert_str, values)
    except Exception as e:
        print "failed to insert: {}, {}\n".format (insert_str, values)
        print str(e)
        sys.exit(0)
        
    values = ()

conn.commit()
conn.close()
