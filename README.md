This script takes a simple json file in this format:

    [
        {
        "name1": "Value1",
        "name2": "value2"
        },
        {
        "name1": "value3",
        "name2": "value4"
        }
    ]

and creates a table in sqlite (with a name you specify) with columns
name1 & name2, and inserts that data into it.  I made this because I had
several 100MB of json I wanted to lookup from a cgi script, and doing
json.load(100MB file) is too expensive for each web request.

